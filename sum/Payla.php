<?php

class Payla implements PaylaMetods
{
    private $array;
    private $wanted;

    public function __construct(array $array =null, int $wanted = null)
    {

        $this->array = $array;
        $this->wanted = $wanted;
    }

    /**
     * @param mixed $array
     */
    public function setArray(array $array): void
    {
        $this->array = $array;
    }

    /**
     * @param mixed $wanted
     */
    public function setWanted(int $wanted): void
    {
        $this->wanted = $wanted;
    }

    /**
     * return matched indexes
     *
     * @return array
     */
    public function getSum(): array
    {
        $length = count($this->array) - 1;
        for ($i = 0; $i < $length; $i++) {
            for ($j = $i; $j <= $length; $j++) {

                if ($this->isBiger($i, $j)) break;
                if ($this->isEqual($i, $j)) return [$i, $j];

            }
        }
        return [0, 0];
    }


    /**
     * @param int $i
     * @param int $j
     * @return bool
     */
    private function isBiger(int $i, int $j): bool
    {
        return ($this->array[$i] + $this->array[$j]) > $this->wanted;
    }

    /**
     * @param int $i
     * @param int $j
     * @return bool
     */
    private function isEqual(int $i, int $j): bool
    {
        return ($this->array[$i] + $this->array[$j]) == $this->wanted;
    }
}
interface PaylaMetods{

   public function setArray(array $array): void;

    public function setWanted(int $wanted): void;

    public function getSum(): array;
}