<?php

require_once 'Payla.php';

class Test
{
    /**
     * @var PaylaMetods
     */
    private $object;

    public function __construct(PaylaMetods $object)
    {
        $this->object = $object;
    }

    public function fire()
    {
        $obj = $this->object;
        $arr = $this->getArray();

        $i = rand(0, count($arr) - 1);
        $j = rand(0, count($arr) - 1);

        $wanted = $arr[$i] + $arr[$j];

        $obj->setWanted($wanted);
        $obj->setArray($arr);

        $result = $obj->getSum();

        assert(is_array($result) == true, 'result of getSum method must be array');

        assert($arr[$result[0]] + $arr[$result[1]] == $arr[$j] + $arr[$i], "result not match");
    }

    /**
     * @return array
     */
    private function getArray(): array
    {
        $arr = [];
        $len = rand(20, 100);

        for ($i = 0; $i < $len; $i++) {
            $arr[] = rand(1000, 1100);
        }

        $arr = array_unique($arr);
        sort($arr);
        return $arr;
    }
}