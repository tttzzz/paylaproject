<?php

/**
 * @param array $arr
 * @return mixed
 */
function detect(array $arr)
{
    $tmp = [];
    foreach ($arr as $key => $value) {

        $check = $tmp[$value] ?? false;

        if ($check === true) {
            return $value;
            break;
        }

        $tmp[$value] = true;
    }
}

$arr = [2,6,8,7,6,1,13,48,5,6,15, 6];
$duplicate = detect($arr);

$counter = array_count_values($arr);
echo $duplicate;
assert($counter[$duplicate] >1);
